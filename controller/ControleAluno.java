package controller;

import java.util.ArrayList;
import model.Aluno;

public class ControleAluno {
    private ArrayList<Aluno> listaAlunos;

    public ControleAluno(){
        listaAlunos = new ArrayList<Aluno>();
    }
    public Aluno buscarAlunoNome(String umNome){
        for(Aluno umAluno : listaAlunos){
            if(umAluno.getNome().equalsIgnoreCase(umNome))
                return umAluno;
        }
        return null;
    }

    public Aluno buscarAlunoMatricula(String umaMatricula){
        for(Aluno umAluno : listaAlunos){
            if(umAluno.getMatricula().equalsIgnoreCase(umaMatricula))
                return umAluno;
        }
        return null;
    }

    public void adicionarAluno(Aluno umAluno){
        listaAlunos.add(umAluno);
    }

    public void removerAlunoNome(String umNome){
        Aluno umAluno = buscarAlunoNome(umNome);
        if(umAluno != null)
            listaAlunos.remove(umAluno);
    }

    public void removerAlunoMatricula(String umaMatricula){
        Aluno umAluno = buscarAlunoMatricula(umaMatricula);
        if(umAluno != null)
            listaAlunos.remove(umAluno);
    }

    public ArrayList<Aluno> getListaAlunos() {
        return listaAlunos;
    }

    public void setListaAlunos(ArrayList<Aluno> listaAlunos) {
        this.listaAlunos = listaAlunos;
    }
}
