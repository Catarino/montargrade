package controller;

import java.util.ArrayList;
import model.Disciplina;

public class ControleDisciplina {
    private ArrayList<Disciplina> listaDisciplinas;
    
    public ControleDisciplina(){
        listaDisciplinas = new ArrayList<Disciplina>();
    }

    public Disciplina buscarDisciplina(String umNome){
        for(Disciplina umaDisciplina : listaDisciplinas){
            if(umaDisciplina.getNome().equalsIgnoreCase(umNome))
                return umaDisciplina;
        }
        return null;
    }

    public void adicionarDisciplina(Disciplina umaDisciplina){
        listaDisciplinas.add(umaDisciplina);
    }

    public void removerDisciplina(String umNome){
        Disciplina umaDisciplina = buscarDisciplina(umNome);
        if(umaDisciplina != null)
            listaDisciplinas.remove(umaDisciplina);
    }

    
    public ArrayList<Disciplina> getListaDisciplinas() {
        return listaDisciplinas;
    }

    public void setListaDisciplinas(ArrayList<Disciplina> listaDisciplinas) {
        this.listaDisciplinas = listaDisciplinas;
    }
}
