package controller;

import java.util.ArrayList;
import model.Professor;

public class ControleProfessor {
    private ArrayList<Professor> listaProfessores;

    public ControleProfessor(){
        listaProfessores = new ArrayList<Professor>();
    }

    public Professor buscarProfessor(String umNome){
        for(Professor umProfessor : listaProfessores){
            if(umProfessor.getNome().equalsIgnoreCase(umNome))
                return umProfessor;
        }
        return null;
    }

    public void adicionarProfessor(Professor umProfessor){
        listaProfessores.add(umProfessor);
    }

    public void removerProfessor(String umNome){
        Professor umProfessor = buscarProfessor(umNome);
        if(umProfessor != null)
            listaProfessores.remove(umProfessor);
    }

    public ArrayList<Professor> getListaProfessores() {
        return listaProfessores;
    }

    public void setListaProfessores(ArrayList<Professor> listaProfessores) {
        this.listaProfessores = listaProfessores;
    }
}
