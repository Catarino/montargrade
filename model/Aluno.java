package model;

import java.util.ArrayList;

public class Aluno extends Pessoa{
    private String matricula;
    private String senha;
    private ArrayList<Disciplina> listaDisciplina;

    public Aluno(){
        super();
    }
    public Aluno(String nome, String matricula){
         super();
         setNome(nome);
         this.matricula = matricula;
         listaDisciplina = new ArrayList<Disciplina>();
    }
    public Aluno(String matricula){
        super();
        this.matricula = matricula;
        listaDisciplina = new ArrayList<Disciplina>();
    }
    public Aluno(String matricula, String senha, ArrayList<Disciplina> listaDisciplina){
        super();
        this.matricula = matricula;
        this.senha = senha;
        this.listaDisciplina = listaDisciplina;
    }

    public Disciplina buscarDisciplina(String umNome){
        for(Disciplina umaDisciplina : listaDisciplina){
            if(umaDisciplina.getNome().equalsIgnoreCase(umNome))
                return umaDisciplina;
        }
        return null;
    }

    public void adicionarDisciplina(Disciplina umaDisciplina){
        listaDisciplina.add(umaDisciplina);
    }

    public void removerDisciplina(String umNome){
        Disciplina umaDisciplina = buscarDisciplina(umNome);
        if(umaDisciplina != null)
            listaDisciplina.remove(umaDisciplina);
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public ArrayList<Disciplina> getListaDisciplina() {
        return listaDisciplina;
    }

    public void setListaDisciplina(ArrayList<Disciplina> listaDisciplina) {
        this.listaDisciplina = listaDisciplina;
    }
}
