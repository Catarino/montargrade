package model;

import java.util.ArrayList;

public class Disciplina {
    private String nome;
    private String codigoDisciplina;
    private ArrayList<Turma> listaTurma;

    public Disciplina(){
        
    }
    public Disciplina(String umNome){
        this.nome = umNome;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getCodigoDisciplina() {
        return codigoDisciplina;
    }

    public void setCodigoDisciplina(String codigoDisciplina) {
        this.codigoDisciplina = codigoDisciplina;
    }

    public ArrayList<Turma> getListaTurma() {
        return listaTurma;
    }

    public void setListaTurma(ArrayList<Turma> listaTurma) {
        this.listaTurma = listaTurma;
    }
    
}
