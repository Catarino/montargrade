package model;

import java.util.ArrayList;

public class Pessoa {
    private String nome;
    private String cpf;
    private String rg;
    private ArrayList<String> listaTelefone;

    public Pessoa(){
        
    }
    public Pessoa(String umNome, String umCpf, String umRg, ArrayList<String> umaListaTelefone){
        nome = umNome;
        cpf = umCpf;
        rg = umRg;
        listaTelefone = umaListaTelefone;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public ArrayList<String> getTelefone() {
        return listaTelefone;
    }

    public void setTelefone(ArrayList<String> telefone) {
        this.listaTelefone = telefone;
    }
}
