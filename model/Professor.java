package model;

import java.util.ArrayList;

public class Professor extends Pessoa{
    private ArrayList<Disciplina> disciplinaMinistradas;

    public Professor(){
        super();
        disciplinaMinistradas = new ArrayList<Disciplina>();
    }

    public Professor(String umNome, String umCpf, String umRg, ArrayList<String> umaListaTelefone){
        super(umNome, umCpf, umRg, umaListaTelefone);
        disciplinaMinistradas = new ArrayList<Disciplina>();
    }

    public Disciplina buscarDisciplina(String umNome){
        for(Disciplina umaDisciplina : disciplinaMinistradas){
            if(umaDisciplina.getNome().equalsIgnoreCase(umNome))
                return umaDisciplina;
        }
        return null;
    }

    public void adicionarDisciplina(Disciplina umaDisciplina){
        disciplinaMinistradas.add(umaDisciplina);
    }

    public void removerDisciplina(String umNome){
        Disciplina umaDisciplina = buscarDisciplina(umNome);
        if(umaDisciplina != null)
            disciplinaMinistradas.remove(umaDisciplina);
    }

    public ArrayList<Disciplina> getDisciplinaMinistradas() {
        return disciplinaMinistradas;
    }

    public void setDisciplinaMinistradas(ArrayList<Disciplina> disciplinaMinistradas) {
        this.disciplinaMinistradas = disciplinaMinistradas;
    }
    
}
