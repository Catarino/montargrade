package model;

import java.util.ArrayList;

public class Turma {
    private String nome;
    private ArrayList<Horario> listaHorario;
    private ArrayList<Pessoa> listaProfessor;
    private ArrayList<Aluno> listaAlunos;
    private int vagasTotal = 0;
    private int vagasDisponiveis = 0;
    private int vagasOcupadas = 0;
    
    public Turma(){
        
    }
    public Turma(String nome){
        this.nome = nome;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Pessoa> getListaProfessor() {
        return listaProfessor;
    }

    public void setListaProfessor(ArrayList<Pessoa> listaProfessor) {
        this.listaProfessor = listaProfessor;
    }

    public ArrayList<Aluno> getListaAlunos() {
        return listaAlunos;
    }

    public void setListaAlunos(ArrayList<Aluno> listaAlunos) {
        this.listaAlunos = listaAlunos;
    }
    public int getVagasTotal() {
        return vagasTotal;
    }

    public void setVagasTotal(int vagasTotal) {
        this.vagasTotal = vagasTotal;
    }

    public int getVagasDisponiveis() {
        return vagasDisponiveis;
    }

    public void setVagasDisponiveis(int vagasDisponiveis) {
        this.vagasDisponiveis = vagasTotal - vagasOcupadas;
    }

    public int getVagasOcupadas() {
        return vagasOcupadas;
    }

    public void setVagasOcupadas(int vagasOcupadas) {
        this.vagasOcupadas = vagasOcupadas;
    }

    public ArrayList<Horario> getListaHorario() {
        return listaHorario;
    }

    public void setListaHorario(ArrayList<Horario> listaHorario) {
        this.listaHorario = listaHorario;
    }
    
}
