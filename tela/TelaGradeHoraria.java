/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tela;

import controller.ControleDisciplina;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.Disciplina;
import model.Horario;
import model.Pessoa;
import model.Turma;

/**
 *
 * @author THAIS
 */
public class TelaGradeHoraria extends javax.swing.JFrame {
    
    /**
     * Creates new form TelaGradeHoraria
     */
    private final ArrayList<Disciplina> listaDisciplinasTeste = listaDisciplinas();
    private DefaultListModel professoresListModel = new DefaultListModel();
    private ArrayList<Turma> listaTurmas = new ArrayList<Turma>();
    private ArrayList<Disciplina> listaDisciplinas = new ArrayList<Disciplina>();
    
    public ArrayList listaDisciplinas(){  
   
        ArrayList<Disciplina> listaDisciplinas = new ArrayList<Disciplina>();
        String s[];
        int i;

        try{  

            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\THAIS\\Desktop\\texto.txt"));
            while(br.ready()){
                i = 0;
                String linha = br.readLine();
                s = linha.split("\\t");
                Disciplina umaDisciplina = new Disciplina();
                umaDisciplina.setCodigoDisciplina(s[i++]);
                umaDisciplina.setNome(s[i++]);
                ArrayList<Turma> listaTurma = new ArrayList<Turma>();
                if(s.length > i+1){
                    do{
                        Turma umaTurma = new Turma();
                        umaTurma.setNome(s[i++]);
                        umaTurma.setVagasTotal(Integer.parseInt(s[i++]));
                        umaTurma.setVagasOcupadas(Integer.parseInt(s[i++]));
                        umaTurma.setVagasDisponiveis(Integer.parseInt(s[i++]));
                        ArrayList<Horario> listaHorario = new ArrayList<Horario>();
                        do{
                            String dia = s[i++];
                            String hora = s[i++];
                            Horario umHorario = new Horario(dia,hora);
                            listaHorario.add(umHorario);
                        }while(s[i].equalsIgnoreCase("Segunda") || s[i].equalsIgnoreCase("Terca") || s[i].equalsIgnoreCase("Quarta") || s[i].equalsIgnoreCase("Quinta") || s[i].equalsIgnoreCase("Sexta"));
                        ArrayList<Pessoa> listaProfessor = new ArrayList<Pessoa>();
                        umaTurma.setListaHorario(listaHorario);
                            Pessoa umaPessoa = new Pessoa();
                            umaPessoa.setNome(s[i++]);
                            listaProfessor.add(umaPessoa);
                        umaTurma.setListaProfessor(listaProfessor);
                        listaTurma.add(umaTurma);
                    }while(i < s.length);
                }
                umaDisciplina.setListaTurma(listaTurma);
                listaDisciplinas.add(umaDisciplina);

            }
             br.close();
          }catch(IOException ioe){  
          }  
        return listaDisciplinas;
    }
    
    public void iniciaGrade(){
        jTextFieldCodigoDisciplina.setEditable(false);
        jTextFieldNomeDisciplina.setEditable(false);
        jComboBoxTurma.setEditable(false);
        jTabbedPaneInformarcoes.setEnabled(false);
        jTableListaDisciplinas.setEnabled(true);
        jButtonAdicionar.setEnabled(false);
        jTextFieldVagas.setEditable(false);
        jTextFieldDisponivel.setEditable(false);
        jTextFieldOcupada.setEditable(false);
        jButtonRemover.setEnabled(false);
    }
    public void editarGrade(){
        jTextFieldNome.setEditable(false);
        jTextFieldMatricula.setEditable(false);
        jButtonAcessar.setEnabled(false);
        jButtonCancelar.setEnabled(false);
        jTextFieldCodigoDisciplina.setEditable(false);
        jTextFieldNomeDisciplina.setEditable(false);
        jComboBoxTurma.setEditable(false);
        jTabbedPaneInformarcoes.setEnabled(true);
        jTableListaDisciplinas.setEnabled(true);
        jButtonAdicionar.setEnabled(true);
        jButtonRemover.setEnabled(true);
    }
    
    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private void carregarListaDisciplinas() throws IOException {
        
        DefaultTableModel model = (DefaultTableModel) jTableListaDisciplinas.getModel();
        model.setRowCount(0);
        for (Disciplina b : listaDisciplinasTeste) {
            model.addRow(new String[]{b.getCodigoDisciplina(), b.getNome()});
        }
    }
    
    public boolean validarCampos(){
        if (jTextFieldNome.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Nome' não foi informado.");
            jTextFieldNome.requestFocus();
            return false;
        }else if(jTextFieldMatricula.getText().trim().length() == 0){
            this.exibirInformacao("O valor do campo 'Matricula' não foi informado.");
            return false;
        }
        return true;
    }
    public TelaGradeHoraria() throws IOException {
        initComponents();
        iniciaGrade();
        carregarListaDisciplinas();
        
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelNome = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabelMatricula = new javax.swing.JLabel();
        jTextFieldMatricula = new javax.swing.JTextField();
        jButtonAcessar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListaDisciplinas = new javax.swing.JTable();
        jTabbedPaneInformarcoes = new javax.swing.JTabbedPane();
        jPanelInformacoes = new javax.swing.JPanel();
        jLabelNomeDisciplina = new javax.swing.JLabel();
        jTextFieldNomeDisciplina = new javax.swing.JTextField();
        jLabelCodigoDisciplina = new javax.swing.JLabel();
        jTextFieldCodigoDisciplina = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListProfessor = new javax.swing.JList();
        jComboBoxTurma = new javax.swing.JComboBox();
        jLabelTurma = new javax.swing.JLabel();
        jLabelVagas = new javax.swing.JLabel();
        jTextFieldVagas = new javax.swing.JTextField();
        jLabelDisponivel = new javax.swing.JLabel();
        jTextFieldDisponivel = new javax.swing.JTextField();
        jLabelOcupada = new javax.swing.JLabel();
        jTextFieldOcupada = new javax.swing.JTextField();
        jLabelDia = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableHorario = new javax.swing.JTable();
        jPanelGradeHoraria = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableGradeHoraria = new javax.swing.JTable();
        jButtonBuscar = new javax.swing.JButton();
        jButtonAdicionar = new javax.swing.JButton();
        jButtonRemover = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabelNome.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelNome.setText("Nome:");

        jTextFieldNome.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabelMatricula.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelMatricula.setText("Matricula:");

        jTextFieldMatricula.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jButtonAcessar.setText("ACESSAR");
        jButtonAcessar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAcessarActionPerformed(evt);
            }
        });

        jButtonCancelar.setText("CANCELAR");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jTableListaDisciplinas.setModel(new javax.swing.table.DefaultTableModel 
            (
                null,
                new String [] {
                    "Codigo da Disciplina", "Nome"
                }
            )
            {
                @Override    
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jTableListaDisciplinas.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jTableListaDisciplinasMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(jTableListaDisciplinas);

            jLabelNomeDisciplina.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
            jLabelNomeDisciplina.setText("Nome:");

            jTextFieldNomeDisciplina.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

            jLabelCodigoDisciplina.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
            jLabelCodigoDisciplina.setText("Codigo Disciplina:");

            jTextFieldCodigoDisciplina.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
            jTextFieldCodigoDisciplina.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jTextFieldCodigoDisciplinaActionPerformed(evt);
                }
            });

            jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
            jLabel1.setText("Professor:");

            jScrollPane2.setViewportView(jListProfessor);

            jComboBoxTurma.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mousePressed(java.awt.event.MouseEvent evt) {
                    jComboBoxTurmaMousePressed(evt);
                }
            });
            jComboBoxTurma.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jComboBoxTurmaActionPerformed(evt);
                }
            });

            jLabelTurma.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
            jLabelTurma.setText("Turma");

            jLabelVagas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
            jLabelVagas.setText("Vagas:");

            jTextFieldVagas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

            jLabelDisponivel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
            jLabelDisponivel.setText("Disponivel:");

            jTextFieldDisponivel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

            jLabelOcupada.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
            jLabelOcupada.setText("Ocupada:");

            jTextFieldOcupada.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

            jLabelDia.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
            jLabelDia.setText("                                                  Horário");

            jTableHorario.setModel(new javax.swing.table.DefaultTableModel 
                (
                    null,
                    new String [] {
                        "Dia", "Hora"
                    }
                )
                {
                    @Override    
                    public boolean isCellEditable(int rowIndex, int mColIndex) {
                        return false;
                    }
                });
                jScrollPane4.setViewportView(jTableHorario);

                javax.swing.GroupLayout jPanelInformacoesLayout = new javax.swing.GroupLayout(jPanelInformacoes);
                jPanelInformacoes.setLayout(jPanelInformacoesLayout);
                jPanelInformacoesLayout.setHorizontalGroup(
                    jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                        .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                                .addComponent(jLabelOcupada)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldOcupada, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                                .addComponent(jLabelVagas)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldVagas, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                                .addComponent(jLabelDisponivel)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldDisponivel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(34, 34, 34))
                                    .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                        .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel1)
                                            .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                                .addComponent(jLabelTurma)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jComboBoxTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane4)
                                    .addComponent(jScrollPane2)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelInformacoesLayout.createSequentialGroup()
                                .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelCodigoDisciplina)
                                    .addComponent(jLabelNomeDisciplina))
                                .addGap(27, 27, 27)
                                .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextFieldCodigoDisciplina, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldNomeDisciplina, javax.swing.GroupLayout.PREFERRED_SIZE, 509, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabelDia, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 82, Short.MAX_VALUE)))
                        .addContainerGap())
                );
                jPanelInformacoesLayout.setVerticalGroup(
                    jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNomeDisciplina)
                            .addComponent(jTextFieldNomeDisciplina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCodigoDisciplina)
                            .addComponent(jTextFieldCodigoDisciplina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addComponent(jLabelDia)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelInformacoesLayout.createSequentialGroup()
                                .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jComboBoxTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelTurma))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelVagas)
                                    .addComponent(jTextFieldVagas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelOcupada)
                                    .addComponent(jTextFieldOcupada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelDisponivel)
                                    .addComponent(jTextFieldDisponivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(17, Short.MAX_VALUE))
                );

                jTabbedPaneInformarcoes.addTab("Informações", jPanelInformacoes);

                jTableGradeHoraria.setModel(new javax.swing.table.DefaultTableModel(
                    new Object [][] {
                        {"08:00", null, null, null, null, null},
                        {"10:00", null, null, null, null, null},
                        {"12:00", null, null, null, null, null},
                        {"14:00", null, null, null, null, null},
                        {"16:00", null, null, null, null, null},
                        {"18:00", null, null, null, null, null}
                    },
                    new String [] {
                        "Hora", "Segunda", "Terca", "Quarta", "Quinta", "Sexta"
                    }
                ) {
                    Class[] types = new Class [] {
                        java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
                    };
                    boolean[] canEdit = new boolean [] {
                        false, false, false, false, false, false
                    };

                    public Class getColumnClass(int columnIndex) {
                        return types [columnIndex];
                    }

                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return canEdit [columnIndex];
                    }
                });
                jTableGradeHoraria.setColumnSelectionAllowed(true);
                jTableGradeHoraria.addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        jTableGradeHorariaMouseClicked(evt);
                    }
                });
                jScrollPane3.setViewportView(jTableGradeHoraria);
                jTableGradeHoraria.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                if (jTableGradeHoraria.getColumnModel().getColumnCount() > 0) {
                    jTableGradeHoraria.getColumnModel().getColumn(0).setMinWidth(50);
                    jTableGradeHoraria.getColumnModel().getColumn(0).setPreferredWidth(50);
                    jTableGradeHoraria.getColumnModel().getColumn(0).setMaxWidth(50);
                }

                javax.swing.GroupLayout jPanelGradeHorariaLayout = new javax.swing.GroupLayout(jPanelGradeHoraria);
                jPanelGradeHoraria.setLayout(jPanelGradeHorariaLayout);
                jPanelGradeHorariaLayout.setHorizontalGroup(
                    jPanelGradeHorariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelGradeHorariaLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE)
                        .addContainerGap())
                );
                jPanelGradeHorariaLayout.setVerticalGroup(
                    jPanelGradeHorariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelGradeHorariaLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
                        .addContainerGap())
                );

                jTabbedPaneInformarcoes.addTab("Grade Horaria", jPanelGradeHoraria);

                jButtonBuscar.setText("BUSCAR");
                jButtonBuscar.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButtonBuscarActionPerformed(evt);
                    }
                });

                jButtonAdicionar.setText("ADICIONAR");
                jButtonAdicionar.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButtonAdicionarActionPerformed(evt);
                    }
                });

                jButtonRemover.setText("REMOVER");
                jButtonRemover.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButtonRemoverActionPerformed(evt);
                    }
                });

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTabbedPaneInformarcoes)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelMatricula)
                                    .addComponent(jLabelNome))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTextFieldMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(12, 12, 12)
                                        .addComponent(jButtonAcessar, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jButtonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jButtonAdicionar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButtonRemover, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(jTextFieldNome))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
                layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNome, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelMatricula)
                            .addComponent(jTextFieldMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonAcessar)
                            .addComponent(jButtonBuscar)
                            .addComponent(jButtonCancelar)
                            .addComponent(jButtonAdicionar)
                            .addComponent(jButtonRemover))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTabbedPaneInformarcoes, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                );

                pack();
            }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldCodigoDisciplinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCodigoDisciplinaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCodigoDisciplinaActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        // TODO add your handling code here:
        jTextFieldNome.setText("");
        jTextFieldMatricula.setText("");
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonAcessarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAcessarActionPerformed
        // TODO add your handling code here:
        if(validarCampos())
            editarGrade();
    }//GEN-LAST:event_jButtonAcessarActionPerformed
    public Disciplina buscaDisciplina(Disciplina umaDisciplina){
        for(int i = 0; i < listaDisciplinasTeste.size(); i++){
            if(umaDisciplina.getNome() != null && umaDisciplina.getNome().equalsIgnoreCase(listaDisciplinasTeste.get(i).getNome()))
                return listaDisciplinasTeste.get(i);
            else if(umaDisciplina.getCodigoDisciplina() != null && umaDisciplina.getCodigoDisciplina().equalsIgnoreCase(listaDisciplinasTeste.get(i).getCodigoDisciplina()))
                return listaDisciplinasTeste.get(i);
        }
        return null;
    }
    public Disciplina buscaDisciplina(String umNome){
        for(int i = 0; i < listaDisciplinasTeste.size(); i++){
            if(umNome != null && umNome.equalsIgnoreCase(listaDisciplinasTeste.get(i).getNome()))
                return listaDisciplinasTeste.get(i);
            else if(umNome != null && umNome.equalsIgnoreCase(listaDisciplinasTeste.get(i).getCodigoDisciplina()))
                return listaDisciplinasTeste.get(i);
        }
        return null;
    }
    
    
    private void modificaComboBox(Disciplina umaDisciplina){
        for(int i = 0; i < umaDisciplina.getListaTurma().size(); i++){
                jComboBoxTurma.addItem(umaDisciplina.getListaTurma().get(i).getNome());
            }
    }
    
    private void modifica(Disciplina umaDisciplina){
        jTextFieldCodigoDisciplina.setText(umaDisciplina.getCodigoDisciplina());
        jTextFieldNomeDisciplina.setText(umaDisciplina.getNome());
        
    }
    
    private void modificaTableHorarios(Disciplina umaDisciplina){
        DefaultTableModel model = (DefaultTableModel) jTableHorario.getModel();
        if(jComboBoxTurma.getItemCount() > 0){
            int j = jComboBoxTurma.getSelectedIndex();
            if(umaDisciplina.getListaTurma().size() > 0){
                model.setRowCount(0);
                for (int i = 0; i < umaDisciplina.getListaTurma().get(j).getListaHorario().size(); i ++) {
                    model.addRow(new String[]{umaDisciplina.getListaTurma().get(j).getListaHorario().get(i).getDia(), umaDisciplina.getListaTurma().get(j).getListaHorario().get(i).getHora()});
                }
                jTextFieldVagas.setText(Integer.toString(umaDisciplina.getListaTurma().get(j).getVagasTotal()));
                jTextFieldOcupada.setText(Integer.toString(umaDisciplina.getListaTurma().get(j).getVagasOcupadas()));
                jTextFieldDisponivel.setText(Integer.toString(umaDisciplina.getListaTurma().get(j).getVagasDisponiveis()));
                
                jListProfessor.setModel(professoresListModel);
                professoresListModel.addElement(umaDisciplina.getListaTurma().get(j).getListaProfessor().get(0).getNome());
            }
        }else{
                model.setRowCount(0);
                jTextFieldVagas.setText("");
                jTextFieldOcupada.setText("");
                jTextFieldDisponivel.setText("");
        }
    }
    
    private void jButtonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBuscarActionPerformed
        // TODO add your handling code here:
        TelaBuscaDisciplina novaBusca = new TelaBuscaDisciplina(this,true);
        novaBusca.setVisible(true);
        Disciplina umaDisciplina = novaBusca.getDisciplina();
        umaDisciplina = buscaDisciplina(umaDisciplina);
        if(umaDisciplina != null){
            jComboBoxTurma.removeAllItems();
            modifica(umaDisciplina);
            modificaComboBox(umaDisciplina);
            professoresListModel.removeAllElements();
            modificaTableHorarios(umaDisciplina);
        }else
            this.exibirInformacao("Disciplina não encontrada, certifique-se de não utilizar caracteres especiais.");
        
        
    }//GEN-LAST:event_jButtonBuscarActionPerformed

    private void jTableListaDisciplinasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaDisciplinasMouseClicked
        // TODO add your handling code here:
        jComboBoxTurma.removeAllItems();
        DefaultTableModel model = (DefaultTableModel) jTableListaDisciplinas.getModel();
        Disciplina umaDisciplina = new Disciplina();
        String codigoDisciplina = (String) model.getValueAt(jTableListaDisciplinas.getSelectedRow(), 0);
        String nomeDisciplina = (String) model.getValueAt(jTableListaDisciplinas.getSelectedRow(), 1);
        umaDisciplina.setCodigoDisciplina(codigoDisciplina);
        umaDisciplina.setNome(nomeDisciplina);
        umaDisciplina = buscaDisciplina(umaDisciplina);
        if(umaDisciplina.getNome() != null){
            modifica(umaDisciplina);
            modificaComboBox(umaDisciplina);
            professoresListModel.removeAllElements();
            modificaTableHorarios(umaDisciplina);
        }else
            this.exibirInformacao("Disciplina não encontrada, certifique-se de não utilizar caracteres especiais.");

    
    }//GEN-LAST:event_jTableListaDisciplinasMouseClicked
    
    private void jComboBoxTurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxTurmaActionPerformed
        // TODO add your handling code here:
        Disciplina umaDisciplina = buscaDisciplina(jTextFieldNomeDisciplina.getText());
        if(jComboBoxTurma.getSelectedItem() != null && jComboBoxTurma.getSelectedItem() != "A"){
            professoresListModel.removeAllElements();
            modificaTableHorarios(umaDisciplina);
        }
    }//GEN-LAST:event_jComboBoxTurmaActionPerformed

    private void jComboBoxTurmaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxTurmaMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxTurmaMousePressed

    public boolean validaDisciplina(Disciplina umaDisciplina){
        int posicao = 0;
        if(jComboBoxTurma.getItemCount() <= 0){
            this.exibirInformacao("Dados da Disciplina não cadastrados. Não será possível adicioná-la a sua grade horária.");
            return false;
        }
        for(int i = 0; i < listaDisciplinas.size(); i++){
            for(int j = 0; j < listaDisciplinas.get(i).getListaTurma().size(); j++){
                if(listaDisciplinas.get(i).getListaTurma().get(j).getNome().equalsIgnoreCase(listaTurmas.get(i).getNome()))
                    posicao = j;
            }
            if(umaDisciplina.getNome().equalsIgnoreCase(listaDisciplinas.get(i).getNome())){
                this.exibirInformacao("Disciplina já cadastrada.");
                return false;
            }
            for(int j = 0; j < umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().size(); j++){
                for(int k = 0; k < listaDisciplinas.get(i).getListaTurma().get(posicao).getListaHorario().size(); k++){
                    if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getDia().equalsIgnoreCase(listaDisciplinas.get(i).getListaTurma().get(posicao).getListaHorario().get(k).getDia()) && umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getHora().equalsIgnoreCase(listaDisciplinas.get(i).getListaTurma().get(posicao).getListaHorario().get(k).getHora())){
                        this.exibirInformacao("Choque de horário. Turma não pode ser adicionada.");
                        return false;
                    }
                }
            }
        }
        return true;
    }
    public int verificaLinha(Disciplina umaDisciplina, int j){
        int linha = 0;
        if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getHora().equalsIgnoreCase("08:00    09:50"))
                linha = 0;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getHora().equalsIgnoreCase("10:00    11:50"))
                linha = 1;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getHora().equalsIgnoreCase("12:00    13:50"))
                linha = 2;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getHora().equalsIgnoreCase("14:00    15:50"))
                linha = 3;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getHora().equalsIgnoreCase("16:00    17:50"))
                linha = 4;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getHora().equalsIgnoreCase("18:00    19:50"))
                linha = 5;
            else
                linha = -1;
        return linha;
    }
    public int verificaColuna(Disciplina umaDisciplina, int j){
        int coluna = 0;
        if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getDia().equalsIgnoreCase("Segunda"))
                coluna = 1;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getDia().equalsIgnoreCase("Terca"))
                coluna = 2;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getDia().equalsIgnoreCase("Quarta"))
                coluna = 3;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getDia().equalsIgnoreCase("Quinta"))
                coluna = 4;
            else if(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().get(j).getDia().equalsIgnoreCase("Sexta"))
                coluna = 5;
            else
                coluna = -1;
        return coluna;
    }
    public void adicionaGradeHoraria(Disciplina umaDisciplina){
        DefaultTableModel model = (DefaultTableModel) jTableGradeHoraria.getModel();
        int linha = 0, coluna = 0;
        for(int j = 0; j < umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()).getListaHorario().size(); j++){
            linha = verificaLinha(umaDisciplina, j);
            coluna = verificaColuna(umaDisciplina, j);
            if(coluna != -1 && linha != -1){
                model.setValueAt(umaDisciplina.getNome(),linha,coluna);
            }
        }
    }

    private void jButtonAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarActionPerformed
        // TODO add your handling code here:
        Disciplina umaDisciplina = buscaDisciplina(jTextFieldNomeDisciplina.getText());
        
        if(validaDisciplina(umaDisciplina)){
            listaTurmas.add(umaDisciplina.getListaTurma().get(jComboBoxTurma.getSelectedIndex()));
            listaDisciplinas.add(umaDisciplina);
            adicionaGradeHoraria(umaDisciplina);
        }
        
    }//GEN-LAST:event_jButtonAdicionarActionPerformed

    public void remover(){
        DefaultTableModel model = (DefaultTableModel) jTableGradeHoraria.getModel();
        
    }
    private void jTableGradeHorariaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableGradeHorariaMouseClicked
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jTableGradeHorariaMouseClicked

    public int verificaLista(Disciplina umaDisciplina){
        int i = 0;
        for(i = 0; i < listaTurmas.size(); i++){
            if(umaDisciplina.getNome().equalsIgnoreCase(listaDisciplinas.get(i).getNome())){
                return i;
            }
        }
        return -1;
    }
    public void removeGradeHoraria(Disciplina umaDisciplina, int i){
        DefaultTableModel model = (DefaultTableModel) jTableGradeHoraria.getModel();
        int linha = 0, coluna = 0;
        for(int j = 0; j < listaTurmas.get(i).getListaHorario().size(); j++){
            linha = verificaLinha(umaDisciplina, j);
            coluna = verificaColuna(umaDisciplina, j);
            if(coluna != -1 && linha != -1){
                model.setValueAt("",linha,coluna);
            }
        }
    }
    private void jButtonRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverActionPerformed
        // TODO add your handling code here:
        int i;
        TelaBuscaDisciplina novaBusca = new TelaBuscaDisciplina(this,true);
        novaBusca.setVisible(true);
        Disciplina umaDisciplina = novaBusca.getDisciplina();
        umaDisciplina = buscaDisciplina(umaDisciplina);
        if(umaDisciplina != null){
            i = verificaLista(umaDisciplina);
            if(i != -1){
                removeGradeHoraria(umaDisciplina, i);
                listaTurmas.remove(i);
                listaDisciplinas.remove(umaDisciplina);
            }
        }
    }//GEN-LAST:event_jButtonRemoverActionPerformed
    
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAcessar;
    private javax.swing.JButton jButtonAdicionar;
    private javax.swing.JButton jButtonBuscar;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonRemover;
    private javax.swing.JComboBox jComboBoxTurma;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelCodigoDisciplina;
    private javax.swing.JLabel jLabelDia;
    private javax.swing.JLabel jLabelDisponivel;
    private javax.swing.JLabel jLabelMatricula;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNomeDisciplina;
    private javax.swing.JLabel jLabelOcupada;
    private javax.swing.JLabel jLabelTurma;
    private javax.swing.JLabel jLabelVagas;
    private javax.swing.JList jListProfessor;
    private javax.swing.JPanel jPanelGradeHoraria;
    private javax.swing.JPanel jPanelInformacoes;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPaneInformarcoes;
    private javax.swing.JTable jTableGradeHoraria;
    private javax.swing.JTable jTableHorario;
    private javax.swing.JTable jTableListaDisciplinas;
    private javax.swing.JTextField jTextFieldCodigoDisciplina;
    private javax.swing.JTextField jTextFieldDisponivel;
    private javax.swing.JTextField jTextFieldMatricula;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeDisciplina;
    private javax.swing.JTextField jTextFieldOcupada;
    private javax.swing.JTextField jTextFieldVagas;
    // End of variables declaration//GEN-END:variables
}
